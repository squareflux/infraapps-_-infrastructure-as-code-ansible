<?php
# Fill our vars and run on cli
# $ php -f db-connect-test.php
$dbname = 'testDB';
$dbuser = 'root';
$dbpass = 'P@ssw0rd';
$dbhost = '192.168.220.140';
$connect = mysqli_connect($dbhost, $dbuser, $dbpass) or die("Unable to Connect to '$dbhost'");
mysqli_select_db($connect, $dbname) or die("Could not open the db '$dbname'");
$test_query = "SELECT name FROM test";
$result = mysqli_query($connect, $test_query);
$row = mysqli_fetch_row($result);
printf ("%s",$row[0]);
