# Infrastructure as Code: Ansible
![Ansible all the things meme](https://i.imgur.com/KVgJI0m.png)
## Some background info


### supported platforms
- google cloud: https://googlecloudplatform.github.io/compute-video-demo-ansible/
- aws: https://docs.ansible.com/ansible/2.5/scenario_guides/guide_aws.html
- digitalocean
- Ubuntu
- red hat (duh!)
- centos

### pro's & con's
- pros
  - Easy creation of YAML PlayBook (configuration) file
  - Easy and fast deployment
  - Secure SSH connection
  - Ready to use built in many useful modules
  - Use Python for modules
  - No need for an agent
- cons
  - ssh requirement
  - Slow for big playbooks
  - No support for MacOs or Windows

### approach (Push/Pull? Imperative/Declarative?)
- push
- Declarative

### programming language(s) used
- python

## Very simple example
### Controlled servers
```shell
root@server:-# apt install ssh python2.7
```

### Ansible controller
```shell
# installing obvs
root@server:-# apt install ansible ssh

user@server:-$ ssh-copy-id <username>@<ip of server> # do this for all servers
# optional: test your connection to the server


user@server:-$ mkdir -p setup && cd $_

user@server:-$ vim hosts

[webservers]
192.168.220.150
192.168.220.151

user@server:-$ vim playbook.yml

- hosts: webservers
  user: root
  tasks:
    - name: install nginx
      apt: pkg=nginx state=present

    - name: start nginx every bootup
      service: name=nginx state=started enabled=yes


user@server:-$ ansible-playbook -i hosts playbook.yml 
```



